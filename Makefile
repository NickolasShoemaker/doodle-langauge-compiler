#################################################
# Doodle Makefile                               #
# Nick Shoemaker                                #
#                                               #
#################################################

CXX=g++
CFLAGS=-std=c++11
INCLUDE=-I./scanner
TARGET=./build/main

# First test if there is a place to build to, then build
all: makebuild $(TARGET)

# Create the project
$(TARGET): main.cpp
	$(CXX) $^ -o $@ $(CFLAGS) $(INCLUDE)

# Create build directory if not present
makebuild:
	@mkdir -p build

# Remove items from build directory
clean:
	rm ./build/*

# Command to run compiler
run:
	./build/main

# Command to run compiler on test program
runtest:
	./build/main testprogram.dd