#######################################################################
# Doodle Language Compiler                                            #
# Version alpha 0.1                                                   #
#                                                                     #
# Doodle is a programming language designed as part of a project for  #
# completion of a Computer Science degree.                            #
#######################################################################

# Contributers 2016                                                   #
Nickolas J Shoemaker

# Dependencies                                                        #
glibc and a c++ compiler