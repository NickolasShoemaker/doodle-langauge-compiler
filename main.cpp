/**********************************************************************
* This is the scanner of the Doodle compiler                          *
* Nick Shoemaker 2016                                                 *
*                                                                     *
* g++ main.cpp -o main -std=c++11 -I./scanner                         *
*                                                                     *
*                                                                     *
***********************************************************************/

#include <iostream>
#include <fstream>
#include <vector>

#include "scanner.hpp"

/*
* usage(int,char**)
*
* print info to the user regarding command flags to the compiler
*/
void usage(char* program){
	printf("%s <file>\n", program);
}

/*
* MAIN
*
* The entry of the compiler
*/
int main(int argc, char* argv[]){
	fdsm automata;

	if(argc > 1){
		std::fstream file(argv[1]);
		std::vector<syntax_node> tokens = scanner(file);
		file.close();

		int current_line = 0;
		for(const auto& elem : tokens){
			if(elem.line > current_line){
				current_line = elem.line;
				std::cout << std::endl;
				std::cout << elem.line << ": ";
			}else{
				std::cout << " ";
			}
			std::cout << elem.code << "(" << elem.numeric_id << ")";
		}
		std::cout << std::endl;
	}else{
		std::string command;
		while(command != "exit"){
			std::cout << "Doodle > ";	//print prompt

			//read in and analyse line
			getline(std::cin, command);
			std::vector<syntax_node> output = automata.lexical_analysis(command);

			//output analysis
			for(const auto& elem : output){
				//std::cout << elem << " ";
				handle_token(elem);
			}
			std::cout << std::endl;
		}
	}

	//parse cammand line
	//get project dimentions
	//seperate definitions into working area
	//tokenize
	//parse
	//optimize
	//generate Nasm code
	//pass to Nasm, LD if linking

	return 0;
}