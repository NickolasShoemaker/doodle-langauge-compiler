/**********************************************************************
* This is a definition of the Finite Deterministic State Machine used *
* by the Doodle compiler in the scanning phase.                       *
*                                                                     *
*                                                                     *
*                                                                     *
*                                                                     *
***********************************************************************/
#ifndef FDSM_HPP
#define FDSM_HPP

#include <iostream>
#include <vector>
#include <string>

#define NA -1 // This is the designated code to kick out of the state table, e.g. read a character not fitting with our state
#define FA -2 // The scanner has read a character that should not be in the file

/*
* This table lists what state to go to next from the current given an askii code
*
*/
static int state_transition_table[][128] = {
//				NU, SH, ST, ET, EO, EQ, AK, BL, BS, TB, NL, VT, FF, CR, SO, SI, DL, D1, D2, D3, D4, NK, SY, EB, CN, EM, SB, EC, FS, GS, RS, US, SP,  !,  ",  #,  $,  %,  &,  ',  (,  ),  *,  +,  ,,  -,  .,  /,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  :,  ;,  <,  =,  >,  ?,  //	 		 	 @,  A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  M,  N,  O,  P,  Q,  R,  S,  T,  U,  V,  W,  X,  Y,  Z,  [,  \,  ],  ^,  _,  //  			 `,  a,  b,  c,  d,  e,  f,  g,  h,  i,  j,  k,  l,  m,  n,  o,  p,  q,  r,  s,  t,  u,  v,  w,  x,  y,  z,  {,  |,  },  ~, DL,
/*root    s0*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, 10,  7, 10, 10, 10, 10,  4, 18, 19, 10, 10, NA, 10, 10, 10,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, NA, NA, 10, 10, 10, 10, /*root    s0*/  10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 13, 10, NA, 10, 10, /*root    s0*/  10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 20, 10, 21,  9, FA,

/*num_lit_s1*/	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,  3, NA,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1, NA, NA, NA, NA, NA, NA, /*num_lit_s1*/ 	NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*num_lit_s1*/ 	NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,  2, NA, NA, NA, NA, NA, NA, FA,
/*num_lit_s2*/	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,  2,  2,  2,  2,  2,  2,  2,  2,  2,  2, NA, NA, NA, NA, NA, NA, /*num_lit_s2*/ 	NA,  2,  2,  2,  2,  2,  2, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*num_lit_s2*/ 	NA,  2,  2,  2,  2,  2,  2, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*num_lit_s3*/	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3, NA, NA, NA, NA, NA, NA, /*num_lit_s3*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*num_lit_s3*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,

/*str_lit_s4*/  FA, FA, FA, FA, FA, FA, FA, FA, FA,  4,  4, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA,  4,  4,  4,  4,  4,  4,  4,  6,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, /*str_lit_s4*/   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  5,  4,  4,  4, /*str_lit_s4*/   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, FA,
/*str_lit_s5*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, /*str_lit_s5*/   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, /*str_lit_s5*/   4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4,  4, FA,
/*str_lit_s6*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*str_lit_s6*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*str_lit_s6*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,

/*comment_s7*/  FA, FA, FA, FA, FA, FA, FA, FA, FA,  7,  7, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA,  7,  7,  8,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, /*comment_s7*/   7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, /*comment_s7*/   7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7, FA,
/*comment_s8*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*comment_s8*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*comment_s8*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,

/*generic_s9*/ 	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA,  9,  9,  9,  9,  9,  9,  9, NA, NA,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, /*generic_s9*/ 	 9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, NA,  9, NA,  9,  9, /*generic_s9*/ 	NA,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9,  9, NA,  9, NA,  9, FA,

/*identi_s10*/ 	FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, 10, NA, 10, 10, 10, 10, NA, NA, NA, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, /*identi_s10*/ 	10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 11, 10, NA, 10, 10, /*identi_s10*/ 	10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, NA, 10, NA, 10, FA,
/*list   s11*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, 11, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, NA, NA, NA, NA, NA, NA, /*list   s11*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 12, NA, NA, /*list   s11*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*list   s12*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*list   s12*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*list   s12*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,

/*listlits13*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, 13, 13, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, 13, NA, NA, NA, NA, NA, NA, 15, NA, NA, NA, NA, 13, NA, NA, NA, 13, 13, 13, 13, 13, 13, 13, 13, 13, 13, NA, NA, NA, NA, NA, NA, /*list   s13*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 14, NA, NA, /*list   s13*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*listlits14*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*list   s14*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*list   s14*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,

/*liststr_15*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, 15, 15, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, 15, 15, 15, 15, 15, 15, 15, 17, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, /*liststr_15*/  15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 16, 15, 15, 15, /*liststr_15*/  15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, FA,
/*liststr_16*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, 15, 15, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, /*liststr_16*/  15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, /*liststr_16*/  15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, FA,
/*liststr_17*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, 17, 17, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, 17, NA, NA, NA, NA, NA, NA, 15, NA, NA, NA, NA, 17, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*liststr_17*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, 18, NA, NA, /*liststr_17*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*liststr_18*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*liststr_18*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*liststr_18*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,

/*endparen19*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*endparen19*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*endparen19*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*startcur20*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startcur20*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startcur20*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*endcurly21*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*endcurly21*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*endcurly21*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
/*startpar22*/  FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, FA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startpar22*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, /*startpar22*/  NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, NA, FA,
};

/*
* This holds information about our tokens as we abstract
*/
struct syntax_node{
	std::string code;
	int numeric_id;
	int line;

	syntax_node(std::string c, int ni, int l){
		code = c;
		numeric_id = ni;
		line = l;
	}
};

/*
union syntax_node{
	struct token{
		
	};
	struct non_terminal{
		std::vector<syntax_node*> children;
	};
};
*/

/*
* fdsm Finite Deterministic State Machine or automata
*
* This class handles a state machine for combinations of printable askii
* characters and the tokens that match those patterns
*/
class fdsm{
	int line; //the line the token ends on, we need to store this between calls
public:
	fdsm(){
		line = 1;
	}

	/*
	* Wrapper that doesn't need to handle blocks.
	* This is for the prompt when input is likely small
	*/
	std::vector<syntax_node> lexical_analysis(std::string code){
		int unimportant;
		return std::move(lexical_analysis(code, unimportant));
	}
	/*
	* This function analyses a codes progress though the state machine
	* keeping track of its current and previous state. Using a failure
	* state NA(-1) it determines when a certain token is done and saves
	* the previous state to be returned.
	*
	* TODO: line numbers drift when we change blocks, figure out why.
	*/
	std::vector<syntax_node> lexical_analysis(std::string code, int &last_good_token, bool add_invalid=true){
		std::vector<syntax_node> end_states;
		std::string token_syntax;

		if(add_invalid)
			code.append(" ");	//ensure we reach a final end state

		int prev_state = 0;
		int state = 0;	//start in root state

		int temp = code[0];

		int i = 0;
		last_good_token = i;
		while(i < code.size()){
			prev_state = state;
			state = state_transition_table[prev_state][temp];
			//std::cout << "temp is " << temp << " state is " << state << " prev " << prev_state << std::endl;
			
            if(state == FA){
            	std::string err = "line " + std::to_string(line) + ":nonprintalbe character read by lexer. Askii code " + std::to_string(temp);
                throw(err);
            }

			if(state < 0 && prev_state > 0){			//we hit an invalid character

				syntax_node node_temp(token_syntax, prev_state, line);

				end_states.push_back(node_temp);
				last_good_token = i; //we want char in buffer that was the end of last token
				state = 0;
				prev_state = 0;
				token_syntax.clear();
			}else if(state <= 0 && prev_state == 0){	//start invalid
                if(temp == '\n')
                    line++;
                temp = code[++i];
				state = 0;
				token_syntax.clear();
			}else{
				token_syntax.push_back(code[i]);
                if(temp == '\n')
                    line++;
				temp = code[++i];			            //eat current character
			}
		}

		return std::move(end_states);
	}
};

#endif //FDSM_HPP