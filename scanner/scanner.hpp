/**********************************************************************
* This is the scanner of the Doodle compiler                          *
* Nick Shoemaker 2016                                                 *
*                                                                     *
*                                                                     *
*                                                                     *
*                                                                     *
***********************************************************************/
#ifndef SCANNER_HPP
#define SCANNER_HPP

#include <iostream>
#include <vector>
#include <fstream>
#include <unordered_map>

#include <cstdio>

#include "fdsm.hpp"

/*
* token
*
* The numeric codes that represent each type. Currently they are set to
* the accept state that they will end in matching their patern for easy
* transfer from out fdsm.
*/
enum token{
	eof_token = -2,			// mark end of file
	scanner_error_noprint = -1,
	scanner_error = 0,

	int_literal = 1,		// 13
	hex_literal = 2,		// 0xfa
	dec_literal = 3,		// 13.5

	keyword = 4,			// use the space between number and string literals to save these
	type = 5,				// special kinds of identifiers

	string_literal = 6,		// 'string literal'

	comment	= 8,			// ".*" we can use this for doc strings and such if desired

	type_generic = 9,		// ~identifier
	identifier = 10,

	//list_generic,			// ~type_keyword[ \d* ]
	list = 12,				// type_keyword[ \d* ]

	list_literal = 14,		// [1, 2, 3]
	list_str_literal = 18,	// ['h', 'i']

	end_paren = 19,
	start_functional = 20,
	end_functional = 21,
	start_paren = 22,
};

std::unordered_map<std::string,int> keywords = {
	//control flow
	{"if", 4},
	{"while", 4},
	{"table", 4},
	{"for", 4},

	//functionality
	{"returns", 4},
	{"return", 4},
	{"contains", 4},
	{"clone", 4},
};

std::unordered_map<std::string,int> types = {
	{"int", 5},
	{"uint", 5},
	{"askii", 5},
	{"float", 5},
	{"operator", 5},
};

/*
* void handle_token(int)
*
* currently this just prints a conformation of each type but later can do
* things like check identifiers for keywords or operator definitions
*/
void handle_token(syntax_node node){
	switch(node.numeric_id){
		case int_literal:
			printf("Scanned integer type\n");
			break;
		case hex_literal:
			printf("Scanned hexadecimal type\n");
			break;
		case dec_literal:
			printf("Scanned decimal type\n");
			break;
		case string_literal:
			printf("Scanned string type\n");
			break;
		case comment:
			printf("Scanned comment type\n");
			break;
		case type_generic:
			printf("Scanned generic type\n");
			break;
		case identifier:
			if(keywords.count(node.code) > 0){
				node.numeric_id = 4;
				printf("Scanned keyword %s\n", node.code.c_str());
			}else if(types.count(node.code) > 0){
				node.numeric_id = 5;
				printf("Scanned type %s\n", node.code.c_str());
			}else{
				printf("Scanned identifier type\n");
			}
			break;
		case list:
			printf("Scanned list identifier type\n");
			break;
		case list_literal:
			printf("Scanned list literal type\n");
			break;
		case list_str_literal:
			printf("Scanned list str literal type\n");
			break;
		case start_paren:
			printf("Scanned start parenthasis\n");
			break;
		case end_paren:
			printf("Scanned end parenthasis\n");
			break;
		case start_functional:
			printf("Scanned start functionality\n");
			break;
		case end_functional:
			printf("Scanned end functionality\n");
			break;
		case scanner_error_noprint:
			printf("Scanner error read no print character (askii 0-32).\n");
			exit(1);
		case scanner_error:
		default:
			printf("Scanner error, improper input.\n");
			exit(1);
	}
}

/*
*
*
* This is where we would like to mode the scanning process from main when
* we get farther
*/
std::vector<syntax_node> scanner(std::fstream& file){
	int buffer_size = 1000;
	std::string buffer;
	char tempbuffer[buffer_size];
	std::vector<syntax_node> tokens;
	fdsm automata;

	try{
		int last_good_token = buffer_size;
		std::string overflow;
		overflow.clear();
		while(file.good()){	//no error or eof

			if(buffer_size == last_good_token)
				overflow = "";
			else
				overflow.assign(tempbuffer + last_good_token, buffer_size - last_good_token);

			file.read(tempbuffer, last_good_token); //what if we cant read that much?
			int num_read = file.gcount();

			if(!overflow.empty())
				buffer = overflow + tempbuffer;
			else
				buffer = tempbuffer;

			buffer.resize(overflow.size() + num_read);
			std::vector<syntax_node> temp = automata.lexical_analysis(buffer, last_good_token, file.eof());
			tokens.insert(tokens.end(),temp.begin(),temp.end());
		}
		syntax_node end("EOF",eof_token,0);
		tokens.push_back(end);
	}catch(std::string exception){
		std::cout << "Exception:" << exception << std::endl;
	}

	return std::move(tokens);
}

#endif //SCANNER